<div class="login-form-wrap">
<?php
drupal_set_title("Login");
print render(drupal_get_form("user_login"));
?>
<div class="register-link">
Don't have credentials? Please <a href="/lean/user/register">Register</a>. 
</div>
</div>