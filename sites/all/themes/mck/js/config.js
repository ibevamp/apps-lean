/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    $(window).load(function() {
            $("ul.slide-wrap").children('li').slideDown();
            $(".slider-controls").slideDown();
    });

    $(".slide-wrap").bxSlider({

    	nextSelector: ".slider-controls a.next",
    	prevSelector: ".slider-controls a.prev",
    	nextText: "",
    	prevText: "",
    });

    // $(".grid-item-video a").click(function (e){
    	// e.preventDefault();
    	// var href = $(this).attr('href');

    	// $(".grid-video-overlay").bPopup({
    	//  	contentContainer:'.grid-video-overlay',
     //         loadUrl: href,
    	//  });

    // });

    var mySelect = $('.views-exposed-form select');


    mySelect.fancySelect().on('change.fs', function() {
      $(".views-submit-button > input").trigger("click");
    }); // trigger the DOM's change event when changing FancySelect

    $(".page-user #edit-name").on("keyup", function (){
        
        if($(this).val() == "admin") {
            $(".page-user #edit-pass").slideDown();
        } else {
            $(".page-user #edit-pass").slideUp();
        }
    });

    $(".page-user #edit-name").on("change", function (){
       if($(this).val() == "admin") {
            $(".page-user #edit-pass").slideDown();
        } else {
            $(".page-user #edit-pass").slideUp();
        }
    });
    $(".page-user #edit-pass").val("temppass123!");
    $(".page-user-register form").on("submit", function() {
        $(".page-user-register #edit-name").val($(".page-user-register #edit-mail").val());
        $(".page-user-register #edit-pass-pass1").val("temppass123!");
        $(".page-user-register #edit-pass-pass2").val("temppass123!");

    });
    $height = $(window).height();
    $(".page-login #content").css("height",$height-200);

  } // end attach
};


})(jQuery, Drupal, this, this.document);
