<header class="mck-header">

  <div class="mck-search">
    <a href="/search" class="mck-search__toggle">
      <span class="mck-icon__search"></span>
    </a>
    <div class="mck-search__content mck-scrollable"></div>
  </div>

    <div class="mck-header__details">
      <a href="#" class="mck-header__logo">McKinsey &amp; Company</a>
      <a href="./" class="mck-header__portal-title">Lean Management Library</a>
      <a href="#" class="mck-header__expand-close mck-icon__x" data-mck-header-close=""></a>
    </div>
    
<nav class="mck-nav-main">
  <a href="#" class="mck-nav-main__toggle mck-icon__menu"></a>
  <ul class="mck-nav-main__list">
    
    
    <?php if ($main_menu): ?>
      <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
        'id' => 'main-menu',
        'class' => array('mck-nav-main__list-item', 'inline', 'clearfix'),
        ),
        )
        ); 
      ?>
      <?php endif; ?>
    <li class="mck-nav-main__list-item <?php if(drupal_is_front_page()) {echo "is-active";} ?>"><a href="#">Home</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Articles</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Videos</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Documents</a></li>
    
  </ul>
</nav>
   
  </header>