  <!--footer-->
  <footer class="mck-footer" role="footer">
  <a href="#" class="mck-footer__logo">McKinsey &amp; Company</a>
  <p class="mck-footer__copy">Suggestions? <a href="mailto:uig_feedback@mckinsey.com">Send us feedback</a></p>
  </footer>
  <!--footer-->
