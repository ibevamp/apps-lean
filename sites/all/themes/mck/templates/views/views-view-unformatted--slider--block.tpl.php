<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<ul class="slide-wrap">
<?php foreach ($rows as $id => $row): ?>
  <li class="slide">
    <?php print $row; ?>
  </li>
<?php endforeach; ?>
</ul>
