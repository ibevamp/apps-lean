<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
// kpr($view);
// kpr($row);

 $content_type = $row->node_type;
$ct = "";
$color = "";
if ($content_type == "videos") {
   $ct =  "<i class='fa fa-video-camera'></i> ". ucfirst($content_type);
}
else {
  $ct = "<i class='fa fa-file-text'></i> ". ucfirst($content_type);
  $color = "white";
}
?>

<div class="grid-item-video"><a href="<?php echo drupal_get_path_alias('node/'.$row->nid); ?>"><?php print render($row->field_field_video_image); ?></a></div>
<div class="grid-item-content">
<div class="grid-summary"><span class="capitalize"><?php echo $ct; ?></span> | <?php print $row->field_field_published_date[0]['rendered']['#markup']; ?></div>
<div class="grid-category <?php echo $color; ?> "><a href=<?php print $row->field_field_category[0]['rendered']['#href']; ?>><?php echo $row->field_field_category[0]['rendered']['#title'] ?></a></div>
<h5 class="grid-item-title <?php echo $color; ?>">
	<a href="
	<?php 
	if($content_type=='documents') { 
		echo $row->field_field_document_upload[0]['rendered']['#markup']; 
	}
	else {
		echo drupal_get_path_alias('node/'.$row->nid);
	} 
	?>
	"><?php print $row->node_title; ?></a></h5>
<div class="grid-item-desc"><?php //print $row->field_field_description; ?></div>
<div class="grid-time"><?php echo $row->field_field_run_time[0]['rendered']['#markup']; ?></div>
</div>