<?php //tpl for grid view
	//kpr($view);
	$colors = array("mck-blue", "bright-blue", "global-blue", "cyan", "teal", "turquoise", "blue-gray", "purple");
	$last_row = count($rows) -1;    // last row
  	$wrapper  = 3;                  // put a wrapper around every 4 nodes.
?>

<div class="grid-wrap">
<?php foreach ($rows as $id => $row): ?>
  
  
	 <?php 
	if ($id % $wrapper == 0) 
	{
	  print '<div class="row grid clearfix">';
	      $i = 0; 
	} ?>
	
	
	  <div class="grid-item type-<?php echo $view->result[$id]->node_type; echo " "; if($view->result[$id]->node_type == "documents") {$rand_key = array_rand($colors,1); echo $colors[$rand_key]; }?>">
	    <?php print $row; ?>
	  </div>
	  
	  
	 <?php 
      $i++; 
      if ($i == $wrapper || $id == $last_row) print '</div>'; 
    ?>
    
	<?php endforeach; ?>
	</div>