<div class="node-video-wrapper">

  <?php $vidurl = file_create_url($variables['field_video_upload'][0]['uri']);
        $vidimg = file_create_url($variables['field_video_image'][0]['uri']);

  ?> 

  <video id="slider" class="video-js vjs-default-skin vjs-big-play-centered"
    controls preload="auto" width="904" height="508"
    poster="<?php echo $vidimg; ?>"
    data-setup='{"example_option":true}'>
   <source src="<?php echo $vidurl; ?>" type='video/mp4' />
   <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
  </video>

  <?php

    $date = $variables['field_published_date'][0]['value'];
    $runningtime = $variables['field_run_time'][0]['value'];
  ?>

  <div class="created-date-running">
    <?php echo $date; ?> | Running Time: <?php echo $runningtime; ?>
  </div>

  <div class="node-video-description">
    <?php echo $variables['field_description'][0]['value']; ?>
  </div>

  <div class="node-video-authors">
    <strong>Authors/Interviewees: </strong><?php echo $variables['field_authors_interviewees'][0]['value']; ?>
  </div>

</div>
