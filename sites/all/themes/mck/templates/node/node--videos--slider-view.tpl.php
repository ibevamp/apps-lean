<?php //kpr($variables); ?>
<div class="slider-left">
  <div class="video-data">
    <i class="fa fa-video-camera"></i>
    <span>Video <span class="vertical-separator">|</span> <span class="video-pub-date"><?php print date('M d , Y', $variables['created']); ?></span></span>
  </div>
  <h3><?php echo $variables['title']; ?></h3>
  <div class="runningtime">
    <strong class="runtime"><?php echo $variables['field_run_time']['und'][0]['value']; ?></strong>
  </div>
</div>
<div class="slider-right">
  <?php $vidurl = file_create_url($variables['field_video_upload']['und'][0]['uri']);
        $vidimg = file_create_url($variables['field_video_image']['und'][0]['uri']);
  ?>

  <video id="slider" class="video-js vjs-default-skin vjs-big-play-centered"
    controls preload="none" width="593" height="334"
    poster="<?php echo $vidimg; ?>"
    data-setup='{"example_option":true}'>
   <source src="<?php echo $vidurl; ?>" type='video/mp4' />
   <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
  </video>

</div>
