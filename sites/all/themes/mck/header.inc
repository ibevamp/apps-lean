<header class="mck-header">

  <div class="mck-search">
    <a href="#" class="mck-search__toggle">
      <span class="mck-icon__search"></span>
    </a>
    <div class="mck-search__content mck-scrollable"></div>
  </div>

    <div class="mck-header__details">
      <a href="#" class="mck-header__logo">McKinsey &amp; Company</a>
      <a href="./" class="mck-header__portal-title">Lean Management Library</a>
      <a href="#" class="mck-header__expand-close mck-icon__x" data-mck-header-close=""></a>
    </div>
    
<nav class="mck-nav-main">
  <a href="#" class="mck-nav-main__toggle mck-icon__menu"></a>
  <ul class="mck-nav-main__list">
    <!-- Primary level item only -->
    <li class="mck-nav-main__list-item"><a href="#">Home</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Articles</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Videos</a></li>
    <li class="mck-nav-main__list-item"><a href="#">Documents</a></li>
    
  </ul>
</nav>
   
  </header>