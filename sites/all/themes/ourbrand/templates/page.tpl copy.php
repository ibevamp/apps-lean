
  <div id="wrapper">
    <!-- Header Starts -->
       <?php print render($page['header']); ?>
    <!-- Header Ends -->
    
    <!-- Main Page Content Starts. -->
    <div id="main">
    
      <h1><?php echo $title; ?></h1> 

      <div id="twocolumns">
          
        <div id="content">
          <?php print render($page['content']); ?>
        </div>
        <div id="sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div>

      </div>
    </div>
    <div id="footer">
        <?php print render($page['footer']); ?>
    </div>
  </div>
  