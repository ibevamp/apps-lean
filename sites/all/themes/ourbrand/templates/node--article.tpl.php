
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	
<?php if (($nid == 98) || ($nid == 210) || ($nid == 212)) { ?>

	<article>
        <div class="article-wrapper">
            
            <div class="article-image">
                <div class="article-image-sizer hero">
					<img class="img-parse" width="700" height="360" src="<?php echo file_create_url($node ->field_hero_image['und'][0]['uri']); ?>" alt="<?php echo $title ?>">
				</div>
                <div class="article-image-caption"></div>
            </div>
            <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
			
			<div class="summary-info">
				<?php print( $node ->body ['und'][0]['summary']); ?>
			</div>
			
			<div class="pub-info">
				<span class="author"><?php echo $node ->field_author_s_['und'][0]['value']; ?>&nbsp;<span class="grey">|</span>&nbsp;</span><span class="pub-date"><?php echo $node ->field_date_time['und'][0]['value']; ?></span>
			</div>
            <?php 
                // We hide the comments and links now so that we can render them later.
                hide($content['comments']);
                hide($content['links']);
                hide($content['field_image']);
                hide($content['field_author_s_']);
                hide($content['field_date_time']);
				hide($content['field_hero_image']);
                print render($content);
             ?>
            
        </div>

         <?php if (!empty($content['links']['terms'])): ?>
           <div class="terms"><?php print render($content['links']['terms']); ?></div>
         <?php endif;?>

    </article>
<?php } else { ?>

    <article>
        <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>

        <div class="pub-info">
            <span class="author"><?php echo $node ->field_author_s_['und'][0]['value']; ?>,&nbsp;</span><span class="pub-date"><?php echo $node ->field_date_time['und'][0]['value']; ?></span>
        </div>

        <?php 
                  // print "<pre>";
                  // print_r($node);
                  // print "</pre>";
                ?>

        <div class="article-wrapper">
            
            <div class="article-image">
                <div class="article-image-sizer">
                    <img class="img-parse" width="400" height="247" src="<?php echo file_create_url($node ->field_image['und'][0]['uri']); ?>" alt="<?php echo $title ?>">
                 </div>
                <div class="article-image-caption"></div>
            </div>
            
            <?php 
                // We hide the comments and links now so that we can render them later.
                hide($content['comments']);
                hide($content['links']);
                hide($content['field_image']);
                hide($content['field_author_s_']);
                hide($content['field_date_time']);
                print render($content);
             ?>
            
        </div>

         <?php if (!empty($content['links']['terms'])): ?>
           <div class="terms"><?php print render($content['links']['terms']); ?></div>
         <?php endif;?>

    </article>
<?php } ?>

</div>
<?php print render($content['comments']); ?>

