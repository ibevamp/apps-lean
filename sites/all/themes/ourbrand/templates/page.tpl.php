<?php 
  $theme_url = variable_get('theme_url', '');
  $base_url = variable_get('base_url', '');
?>
<header><a href="<?php echo $base_url; ?>" class="mck"></a>

    <h1 class="logo"><a href="<?php echo $base_url; ?>"><img alt="SaaS Radar" src="<?php echo $theme_url; ?>/img/sass.png"></a></h1>

    <div class="navbar">
      <?php if ($main_menu || $secondary_menu): ?>
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('nav', 'navbar-nav')))); ?>
     <?php endif; ?>          
    </div>
    <!--navbar-->

    <div id="contact-links"><span><a href="http://www.saasradar.com/user" target="_blank">Login</a></span><a href="mailto:saasradar@mckinsey.com">Contact Us</a>
    </div>

    <!--contact"-->
</header>

<section id="body">
    <section id="content">
        <h3><?php echo $title; ?></h3>
        <section class="featured_stories">
           
           <?php print render($page['content']); ?>
            
        </section>
        <section class="featured_stories noborder">
            
           <?php print render($page['featured']); 
           print $node -> field_sadf ["und"][0]["value"];

           ?>

            
        </section>
        <!--featured_stories-->

        
    </section>
    <!--content-->

    <aside id="main-sidebar">

        <?php print render($page['sidebar_first']); ?>    
      
    </aside>

</section>
<!-- body-->