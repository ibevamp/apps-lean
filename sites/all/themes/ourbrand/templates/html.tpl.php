<?php 
  $theme_url = variable_get('theme_url', '');
  $base_url = variable_get('base_url', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"
  <?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <meta http-equiv="Content-Type"content="text/html; charset=UTF-8">
  <meta http-equiv="x-ua-compatible" content="IE=8">
  <meta name="google-site-verification" content="WwxS_JDPGEJnFBHzRLGSqO2YcyIRc-tejI5p0PQ9uK4">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <link media="all" type="text/css" rel="stylesheet" href="<?php echo $theme_url; ?>/css/styles.css" />
  <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic|Roboto:400,400italic,500,500italic|Open+Sans:400,300' rel='stylesheet' type='text/css'>
  <?php print $styles; ?>

  <!--[if lt IE 9]>
    <script src="<?php echo $theme_url; ?>/script/html5shiv.js"></script>
  <![endif]-->
  <script async="" src="//www.google-analytics.com/analytics.js"></script>
  <script src="<?php echo $theme_url; ?>/lib/jquery-1.9.1.js" type="text/javascript"></script>
  <script src="<?php echo $theme_url; ?>/lib/underscore.js" type="text/javascript"></script>
  <script src="<?php echo $theme_url; ?>/lib/backbone.js" type="text/javascript"></script>

  <?php print $scripts; ?>

  <!--[if lt IE 8]><link rel="stylesheet"type="text/css"href="css/ie8.css"media="screen"/><![endif]-->
</head>

<body id="<?php echo $body_id; ?>" class="main-body <?php print $classes; ?>" <?php print $attributes;?>>

  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

<!-- body-->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-47701271-1', 'saasradar.com');
    ga('send', 'pageview');

</script>

</body>
</html>
