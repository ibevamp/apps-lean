<?php 
  $theme_url = variable_get('theme_url', '');
  $base_url = variable_get('base_url', '');
?>
<header><a href="#" class="mck"></a>

    <h1 class="logo"><a href="#"><img alt="SaaS Radar" src="<?php echo $theme_url; ?>/img/sass.png"></a></h1>
    
    <div class="navbar">
      <?php if ($main_menu || $secondary_menu): ?>
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('nav', 'navbar-nav')))); ?>
     <?php endif; ?>          
    </div>
    <!--navbar-->

    <div id="contact-links"><span><a href="http://saasradar.mckinsey.com/wix/p22215167.aspx" target="_blank">Login</a></span><a href="mailto:saasradar@mckinsey.com">Contact Us</a>
    </div>

    <!--contact"-->
</header>
<section id="body">
    <section id="content"><img alt="Report" src="<?php echo $theme_url; ?>/img/report.jpg" height="205" width="158" border="0" style="float: left;">

<section class="featured_stories front">
    <h3>What is SaaSRadar?</h3>
    <p>SaaSRadar is a proprietary benchmark database of leading business-to-business (B2B) software-as-a-service (SaaS) companies designed to help you identify levers of efficient growth. You may obtain a custom report with benchmarks and best practices from B2B SaaS companies similar to yours. <a href="/get-started">Participation is free,</a> and all data remain confidential.</p>

    <a href="contact-us" class="more link">Download a sample report</a>
    <br><br>
</section>

<?php print render($page['featured']); ?>

<!-- <h3>Articles</h3>
<div class="article-list">
    
        <div class="article-row">
            
                <div class="article-block">
    <a href="#!t_courting-and-cross-selling-the-secrets-of-low-churn-SaaS-firms">
        
            <div class="article-block-image">
                <img width="215" src="http://www.saasradar-blog.com/sites/default/files/styles/small_block/public/field/image/image-0730.png?itok=TIrARbT5" alt="">
            </div>
        
        <div class="article-container">
            <div class="pub-date">July 30, 2014 - 10:42pm</div>
            <div class="article-title">Courting and cross-selling: the secrets of low-churn SaaS firms </div>
            <div class="article-teaser"><p>Our research into SaaS companies serving large and mid-enterprise customers shows
...</p></div>
        </div>
    </a>
</div>
            
                <div class="article-block">
    <a href="#!t_how-much-value-did-you-give-away-to-close-your-last-saas-deal">
        
            <div class="article-block-image">
                <img width="215" src="http://www.saasradar-blog.com/sites/default/files/styles/small_block/public/field/image/image0781.png?itok=oix16gSr" alt="">
            </div>
        
        <div class="article-container">
            <div class="pub-date">July 11, 2014 - 2:43pm</div>
            <div class="article-title">How much value did you give away to close your last SaaS deal? </div>
            <div class="article-teaser"><p>Learn how our analysis of 100 leading SaaS companies shows what works, what doesn’t
...</p></div>
        </div>
    </a>
</div>
            
                <div class="article-block">
    <a href="#!t_b2b-sales-is-being-massively-disrupted">
        
            <div class="article-block-image">
                <img width="215" src="http://www.saasradar-blog.com/sites/default/files/styles/small_block/public/field/image/image003_0.jpg?itok=gIfUwxuC" alt="">
            </div>
        
        <div class="article-container">
            <div class="pub-date">June 16, 2014 - 9:28am</div>
            <div class="article-title">B2B Sales Is Being Massively Disrupted </div>
            <div class="article-teaser"><p>Enterprise sales and marketing models have traditionally been built around large sales forces, C-suite relationships, and enterprise-wide...</p></div>
        </div>
    </a>
</div>
            
        </div>
    
</div>

<a class="more link" href="#!articlelist">More articles</a> -->

<footer>
    <ul id="links">
        <li id="copyright">© 1996-2014 McKinsey &amp; Company</li>
    </ul>
</footer></section>
    <!--content-->

    <aside id="main-sidebar">

        <section><a href="/get-started" class="more">Join the database</a>
            <p><a href="/get-started">Participate now</a><br>You’ll receive a customized report comparing your business with a hand-chosen cohort of similar SaaS companies across 40 critical metrics.</p>
        </section>
        <section><a href="http://saasradar.mckinsey.com/wix/p22215167.aspx" target="_blank" class="more" style="padding-top: 0;">Login to
            the<br>
            data collection</a></section>
    </aside>

</section>
<!-- body-->