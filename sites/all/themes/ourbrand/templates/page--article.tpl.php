<?php 
  $theme_url = variable_get('theme_url', '');
  $base_url = variable_get('base_url', '');
?>
<header><a href="#" class="mck"></a>

    <h1 class="logo"><a href="#"><img alt="SaaS Radar" src="<?php echo $theme_url; ?>/img/sass.png"></a></h1>

    <div class="navbar">
      <?php if ($main_menu || $secondary_menu): ?>
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('nav', 'navbar-nav')))); ?>
     <?php endif; ?>          
    </div>
    <!--navbar-->

    <div id="contact-links"><span><a href="http://saasradar.mckinsey.com/wix/p22215167.aspx" target="_blank">Login</a></span><a href="mailto:saasradar@mckinsey.com">Contact Us</a>
    </div>

    <!--contact"-->
</header>

<section id="body">
    <section id="content">

      <div class="article-full">
          <span class="blog-list"><a href="article-list">List of blogs</a></span>
          <div class="social">
              
              <a href="http://www.facebook.com/share.php?u=<?php echo $_SERVER['HTTP_HOST'] . '/' . request_uri();?>" target="_blank"><img width="25" height="25" alt="Share on Facebook" src="<?php echo $theme_url; ?>/img/facebook-icon.jpg"></a>&nbsp;
              <a href="https://plus.google.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . '/' . request_uri();?>" target="_blank"><img width="25" height="25" alt="Share on Google+" src="<?php echo $theme_url; ?>/img/g-plus-icon.jpg"></a>&nbsp;
              <!-- <a href="http://twitter.com/share?url=<?php echo $_SERVER['HTTP_HOST'] . '/' . request_uri();?>&amp;text=<?php echo $title;?>" target="_blank"><img width="25" height="25" alt="Tweet this" src="<?php echo $theme_url; ?>/img/twitter-icon.jpg"></a> -->
			  <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?php echo 'http%3A%2F%2F'.urlencode($_SERVER['HTTP_HOST'] . '/' . request_uri());?>&text=<?php echo $title; ?>&tw_p=tweetbutton&url=<?php echo 'http%3A%2F%2F'.urlencode($_SERVER['HTTP_HOST'] . '/' . request_uri());?>"><img width="25" height="25" alt="Tweet this" src="<?php echo $theme_url; ?>/img/twitter-icon.jpg"></a>
              <!-- <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $_SERVER['HTTP_HOST'] . '/' . request_uri();?>&amp;title=<?php echo $title;?>&amp;summary=Our research into SaaS companies serving large and mid-enterprise customers shows
              ..." target="_blank"><img width="25" height="25" alt="Tweet this" src="<?php echo $theme_url; ?>/img/linkedin-icon.jpg"></a> -->
			   <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo 'http%3A%2F%2F'.urlencode($_SERVER['HTTP_HOST'] . '/' . request_uri());?>&title=<?php echo $title;?>" target="_blank"><img width="25" height="25" alt="Tweet this" src="<?php echo $theme_url; ?>/img/linkedin-icon.jpg"></a>
			  
          </div>
          <?php print render($page['content']); ?>
          

      </div>
      <div class="article-footer">
          <?php 

            $next = node_sibling('next',$node,NULL,NULL,NULL,FALSE);
            //$previous = node_sibling('previous',$node,NULL,NULL,NULL,FALSE); 
            //print $previous.'||'.$next;
          ?>
          <a class="more link-left" href="article-list">More Articles</a>
          <!-- <a class="more link" href="#!t_how-much-value-did-you-give-away-to-close-your-last-saas-deal"><?php echo $next; ?></a> -->
          <?php echo $next; ?>
      </div>
           
            
       
        
    </section>
    <!--content-->

    <aside id="main-sidebar">

        <?php print render($page['sidebar_first']); ?>    
      
    </aside>

</section>
<!-- body-->