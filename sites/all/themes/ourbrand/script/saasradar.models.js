(function() {
    window.SR = window.SR || {};
    window.SR.Model = window.SR.Model || {};

    SR.Model.Article = Backbone.Model.extend(
        {
            nextArticle: null,
            parse: function(data) {
                if (typeof data == "string") {
                    data = JSON.parse(data);
                }
                this.trimTeaser(data.node);
                return data.node;
            },
            update:function(article) {
                var keys = article.keys();
                var value;
                for (var i = 0; i < keys.length; i++) {
                    value = this.get(keys[i]);
                    if (value) {
                        continue;
                    }
                    this.set(keys[i], article.get(keys[i]));
                }
            },
            trimTeaser: function (data) {
                var titleLength = data["title"].length;

                var length = 160;
                if (titleLength > 55) {
                    length = 100;
                } else if (titleLength > 29) {
                    length = 125;
                }
                var teaser = data["teaser"];
                var end = teaser.substr(length);
                var firstSpace = end.indexOf(" ");
                teaser = teaser.substr(0, length + firstSpace) + "...";
                data["teaser"] = teaser;
            }
        }
    );

    //region Collections

    SR.Model.Articles = Backbone.Collection.extend(
        {
            model:SR.Model.Article,
            secondFetch: false,

            parse: function (response) {
                if (typeof response == "string") {
                    response = JSON.parse(response);
                }
                return response.nodes;
            },

            url: "fullarticle-proxy.aspx"
        }
    );

})();

