(function()
{
    if (!window["EGO7"]) window["EGO7"]		    = {};
    if (!EGO7["Validation"]) EGO7["Validation"]	= {};

    EGO7.Validation.Validator = function(decClass, indClass, markFields, decTarget, indTarget)
    {
        var self        = this;
        var cache      = {};

        this.decClass   = decClass || null;
        this.indClass   = indClass || null;
        this.markFields = markFields == null || markFields == undefined ? null : markFields;
        this.decTarget  = decTarget || null;
        this.indTarget  = indTarget || null;

        this.validateField = function(field, expression, msg)
        {
            return $validateFields([{field:field, expression: expression, msg: msg}]);
        };

        this.validateFields = function(fieldArray)
        {
            return $validateFields(fieldArray);
        };

        this.clearAll = function()
        {
            for(var i in cache) if(cache.hasOwnProperty(i)) unmarkField(i);
        };

        function markField(field, msg)
        {
            var indicator;
            var f        = $(field);
            var indTgt   = self.indTarget ? f.closest(self.indTarget) : f.parent();
            var decTgt   = self.decTarget ? f.closest(self.decTarget) : f;

            if(cache[field])
            {
                cache[field].text(msg);
            }
            else
            {
                decTgt.addClass(self.decClass);
                indicator = $("<div class='" + self.indClass + "' style='white-space: nowrap'>" + msg + "</div>").appendTo(indTgt);
                cache[field] = indicator;
                f.on("focus", onField);
            }
        }

        function onField(e)
        {
            var field = "#" + $(e.target).off("focus", onField).attr("id");
            unmarkField(field);
        }

        function unmarkField(field)
        {
            var f = $(field);
            var indTgt   = self.indTarget ? f.closest(self.indTarget) : f.parent();
            var decTgt   = self.decTarget ? f.closest(self.decTarget) : f;
            decTgt.removeClass(decClass);
            $(cache[field]).remove();
            delete cache[field];
        }

        function $validateFields(fieldArray)
        {
            var o, i;
            var msg     = null;
            var bool    = true;
            var l       = fieldArray.length;
            var fields  = {};

            for(i = 0; i < l; i++)
            {
                o = fieldArray[i];
                if(fields[o.field] == undefined)
                {
                    fields[o.field] = {};
                    fields[o.field].validators = [];
                }
                o.valid = typeof o.expression == "boolean" ? o.expression : new RegExp(o.expression).test($(o.field).val());
                fields[o.field].validators.push({valid:o.valid, msg:o.msg});
            }

            for(i in fields)
            {
                if(fields.hasOwnProperty(i))
                {
                    var b = true;
                    for(var j in fields[i].validators)
                    {
                        if(fields[i].validators.hasOwnProperty(j) && !fields[i].validators[j].valid)
                        {
                            msg     = msg == null ? fields[i].validators[j].msg : msg;
                            bool    = false;
                            b       = false;
                        }
                    }
                    if(self.markFields)
                    {
                        if(!b)
                        {
                            markField(i, msg);
                            msg = null;
                        }
                        else
                        {
                            unmarkField(i);
                        }
                    }
                }
            }
            return bool;
        }
    };

    EGO7.Validation.Regex =
    {
        "EMPTY" : /^\s*\S.*$/,
        "STR_PLAIN" : /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/,
        "NUM_DEC" : /^[0-9]+(\.[0-9]{1,2})?$/,
        /*"NUM_PCT" :/(?!^0*$)(?!^0*\.0*$)^\d{1,2}(\.\d{1,2})?$/*/
        "NUM_PCT" : /^(100(\.0{1,2})?|[1-9]?\d(\.\d{1,2})?)$/,
        "EMAIL" : /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "PASSWORD" : /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})/,
        "CREDITCARD" : /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/
    }
})();