(function ()
{
    if(!window["EGO7"]) window["EGO7"] = {};
    if(!EGO7["UI"]) EGO7["UI"]         = {};

    EGO7.UI.MultiScreen = function(element, props)
    {
        var self                = this,
            mode                = props ? props["mode"] || "img" : "img",
            dataField           = props ? props["dataField"] || "uri" : "uri",
            scale               = props ? props["scale"] || false : false,
            slideShow           = props ? props["slideShow"] || false : false,
            autoPlay            = props ? props["autoPlay"] || true : true,
            interval            = props ? props["interval"] || 10000 : 10000,
            horAlign            = props ? props["horAlign"] || "left" : "left",
            verAlign            = props ? props["verAlign"] || "top" : "top",
            transition          = props ? props["transition"] || null : null,
            arrows              = props ? props["arrows"] || true : true,
            arrowLeftClass      = props ? props["arrowLeftClass"] || "e7-ui-icons-img-container-left-arrow" : "e7-ui-icons-img-container-left-arrow",
            arrowRightClass     = props ? props["arrowRightClass"] || "e7-ui-icons-img-container-right-arrow" : "e7-ui-icons-img-container-right-arrow",
            //screens             = [],
            timer               = null,
            currentScreen       = null,
            oldScreen           = null,
            imgWidth,
            imgHeight,
            leftArrow,
            rightArrow;

        //<editor-fold desc="PUBLIC">

        this.selectedIndex  = null;

        this.loadScreen = function(index)
        {
            this.selectedIndex = index;
            switch(mode)
            {
                case "img":
                    var img             = new Image();
                    img.onload          = onImg;
                    img.onerror         = onImgError;
                    img.style.display   = "block";
                    img.style.position  = "absolute";
                    img.src             = this.data[index][dataField];
                    //screens.unshift($(img));
                    break;
                case "html":
                    if(currentScreen == null)
                    {
                        currentScreen = $(this.data[index][dataField]).css(positionScreen()).hide().appendTo(element).fadeIn(600, onEndTransition);
                    }
                    else
                    {
                        oldScreen = currentScreen;
                        currentScreen = $(this.data[index][dataField]).css(positionScreen()).hide().appendTo(element).fadeIn(600, onEndTransition);
                    }
                    positionScreen();
                    self.element.trigger("change");
                    break;
            }
        };

        this.next = function()
        {
            this.selectedIndex = this.selectedIndex == this.data.length - 1 ? 0 : this.selectedIndex + 1;
            this.loadScreen(this.selectedIndex);
        };

        this.play = function()
        {
            if(!slideShow || timer != null) return;
            timer = setInterval(function() { self.next(); }, interval);
        };

        this.previous = function()
        {
            this.selectedIndex = this.selectedIndex == 0 ? this.data.length-1 : this.selectedIndex - 1;
            this.loadScreen(this.selectedIndex);
        };

        this.resize = function(w, h)
        {
            if(!w && !h)
            {
                this.width  = this.element.width();
                this.height = this.element.height();
            }
            else
            {
                this.width  = w;
                this.height = h;
                this.element.width(w);
                this.element.height(h);
            }

            if(images.length > 0)
            {
                if(scale) scaleImg();
                positionImg();
            }
        };

        this.setData = function(data)
        {
            //for(var i in images) images[i].remove();
            this.selectedIndex  = 0;
            this.data           = data;

            if(data.length == 0) return;
            if(autoPlay)
            {
                this.loadScreen(0);
                if(slideShow) this.play();
            }
        };

        this.setElement = function(element)
        {
            this.selectedIndex  = 0;
            this.element        = element.jquery ? element : $(element);
            self.element.css({"overflow": "hidden", position: "relative" });
            this.width          = this.element.outerWidth();
            this.height         = this.element.outerHeight();
            checkHtml();
            if(arrows) setArrows();
            if(this.element.touchwipe) this.element.touchwipe({
                wipeLeft: function() { self.previous(); },
                wipeRight: function() { self.next(); },
                min_move_x: 20,
                min_move_y: 20,
                preventDefaultEvents: true
            });
            if(props) if(props["data"]) this.setData(props["data"]);
        };

        this.stop = function()
        {
            if(!slideShow || timer == null) return;
            clearInterval(timer);
        };

        //</editor-fold>

        if(element) self.setElement(element);

        //<editor-fold desc="PRIVATE">

        function checkHtml()
        {
            var $list = self.element.find("ul li");
            if($list.length == 0) return;
            var d = [];
            $list.each(function()
            {
                d.push({url:$(this).attr("data-src")});
            });
            //self.element.empty();
            props["data"] = d;
            self.element.find("ul").hide();
        }

        function onEndTransition()
        {
            if(oldScreen) oldScreen.remove();
            element.trigger("endTransition");
            //$(images.pop()).remove();
        }

        function onImgError()
        {
            window.console.log("ON IMG ERROR");
        }

        function onImg()
        {
            if(scale) scaleImg();
            self.element.append(images[0]);

            positionImg();

            if(transition)
            {
                images[0].hide();
                switch(transition.toLowerCase())
                {
                    case "fadein":
                        if(images.length > 1)
                        {
                            images[0].fadeIn();
                            images[1].fadeOut({done:onEndTransition});
                        }
                        else
                        {
                            images[0].fadeIn();
                        }
                        break;
                    case "wipe":
                        var startx, endx, outx;
                        if(horAlign == "left")
                        {
                            startx  = self.element.width();
                            endx    = 0;
                            outx    = images.length > 1 ? -(images[1].width()) : -(images[0].width());
                        }
                        else if(horAlign == "center")
                        {
                            startx  = self.element.width() + "px";
                            endx    = images.length > 1 ? ((self.element.width() - images[1].width())/2) + "px" : ((self.element.width() - images[0].width())/2) + "px";
                            outx    = images.length > 1 ? -(images[1].width()) : -(images[0].width());
                        }
                        if(horAlign == "right")
                        {
                            startx  = 0;
                            endx    = images.length > 1 ? (self.element.width() - images[1].width()) + "px" : (self.element.width() - images[0].width()) + "px";
                            outx    = self.element.width() + "px";
                        }

                        if(images.length > 1)
                        {
                            images[0].css("left", startx).show().animate({left:endx}, 200);
                            images[1].animate({left: outx}, 200, onEndTransition);
                        }
                        else
                        {
                            images[0].css("left", startx).show().animate({left:endx}, 200);
                        }
                        break;
                }
            }
            else
            {
                if(images.length > 1) $(images.pop()).remove();
            }

            self.element.trigger("change");
        }

        function scaleImg()
        {
            var ch,
                imgWidth    = images[0][0].width,
                imgHeight   = images[0][0].height;

            if (self.width > self.height)
            {
                ch = self.height / imgHeight;
                if (imgHeight < imgWidth) ch = ((imgWidth * ch < self.width) ? ch : self.width / imgWidth);
            }
            else
            {
                ch = self.width / imgWidth;
                if (imgWidth < imgHeight) ch = ((imgHeight * ch < self.height) ? ch : self.height / imgHeight);
            }

            images[0][0].height *= ch;
            images[0][0].width *= ch;
        }

        function setArrows()
        {
            leftArrow    = $("<span class='" + arrowLeftClass + "'></span>").appendTo(self.element).css("z-index", "10000").hide();
            rightArrow   = $("<span class='" + arrowRightClass + "'></span>").appendTo(self.element).css("z-index", "10000").hide();
            setupEvents();
        }

        function setupEvents()
        {
            var arrows = $(leftArrow).add(rightArrow);
            self.element.on("mouseenter mouseleave", function(e)
            {
                switch(e.type)
                {
                    case "mouseenter":
                        arrows.fadeIn();
                        break;
                    case "mouseleave":
                        arrows.fadeOut();
                        break;
                }
            });
            leftArrow.click(function() { self.previous(); });
            rightArrow.click(function() { self.next(); });
        }

        function positionScreen()
        {
            var obj = {};
            switch(horAlign)
            {
                case "left":
                    obj.left = 0;
                    break;
                case "right":
                    obj.right = 0;
                    break;
                case "center":
                    obj.left = ((self.width - currentScreen.width)/2) + "px";
                    break;
            }
            switch(verAlign)
            {
                case "top":
                    obj.top = 0;
                    break;
                case "bottom":
                  obj.bottom = 0;
                    break;
                case "middle":
                  obj.top = ((self.height - currentScreen.height)/2) + "px";
                    break;
            }

            return obj;
            //if(arrows) $(leftArrow).add(rightArrow).css("top", ((self.height - 32)/2).toString() + "px");
        }
        //</editor-fold>
    };
}());