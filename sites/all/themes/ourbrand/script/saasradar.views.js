(function() {
    window.SR = window.SR || {};
    window.SR.View = window.SR.View || {};

    SR.View.FrontView = Backbone.View.extend(
        {
            template: Handlebars.getTemplate("blog_front"),
            render: function () {
                if (!SR.Data.firstLoad) {
                    return this;
                }
                var len = Math.min(this.collection.length, 3);

                var data = this.collection.first(len);
                var cells = _.map(data, function(model) {
                    return _.extend({cid: model.cid}, model.attributes);
                });

                var rows = {rows:[{cells: cells}]};
                this.el.html(this.template(rows));

                return this;
            }

        }
    );

    SR.View.ArticleList = Backbone.View.extend(
        {
            template: Handlebars.getTemplate("blog_list"),
            render: function () {
                if (!SR.Data.firstLoad) {
                    return this;
                }
                var data = {rows: []};
                var cells = this.collection.map(function(model) {
                    return _.extend({cid: model.cid}, model.attributes);
                });

                var step = 3;
                var i = 0;
                var len = cells.length;
                for (; i < len; i += step) {
                    data.rows.push({cells:cells.slice(i, i+step)});
                }

                this.el.html(this.template(data));

                return this;
            }
        }
    );

    SR.View.ArticleView = Backbone.View.extend(
        {
            template: Handlebars.getTemplate("blog_article"),
            render: function () {
                if (!window.SR.Data.fullyLoaded) {
                    return this;
                }

                var url = "http://www.saasradar.com/?page=t_"+this.model.get("path");

                if (this.currentPage === SR.Views.articleView) {
                    var index = SR.Data.articles.indexOf(this.currentPage.model) + 1;
                    if (index >= SR.Data.articles.length) {
                        index = 0;
                    }

                    SR.Views.articleView.model = SR.Data.articles.at(index);
                    this.currentPage = null;
                    this.setPage(SR.Views.articleView);
                } else {
                    console.log("nextArticle called but not showing article view!");
                }

                // testing if the following will work when page is rendered
                // via phantom.js on web crawler access.

                $('meta[property=og\\:url]').attr('content', url);
                $('meta[property=og\\:title]').attr('content', this.model.get("title"));
                $('meta[property=og\\:image]').attr('content', this.model.get("large_image")["src"]);
                $('meta[property=og\\:description]').attr('content', this.model.get("teaser"));

                $('meta[property=twitter\\:url]').attr('content', url);
                $('meta[property=twitter\\:title]').attr('content', this.model.get("title"));
                $('meta[property=twitter\\:image]').attr('content', this.model.get("large_image")["src"]);
                $('meta[property=twitter\\:description]').attr('content', this.model.get("teaser"));

                this.el.html(this.template({model: this.model.toJSON(), next_id:this.model.nextArticle, url: url}));

                return this;
            }

        }
    );

    SR.View.StaticView = Backbone.View.extend(
        {
            templates: {
                learn : Handlebars.getTemplate("learn_page"),
                team : Handlebars.getTemplate("team_page"),
                contact : Handlebars.getTemplate("contact_page"),
                getStarted : Handlebars.getTemplate("get_started_page"),
                policy: Handlebars.getTemplate("policy_page"),
                thankyou: Handlebars.getTemplate("thankyou_page")
            },
            setTemplate: function(name) {
                this.template = this.templates[name];
            },
            render: function () {

                this.el.html(this.template({}));

                return this;
            }
        }
    );
})();