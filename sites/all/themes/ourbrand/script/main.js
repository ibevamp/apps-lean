(function()
{
    window.SR = window.SR || {};
    window.SR.Data = window.SR.Data || {
        fullyLoaded: false,
        firstLoad: false
    };
    window.SR.Views = window.SR.Views || {};
    window.console = window.console || {
        log: function(msg) {
            // nada
        }
    };

    SR.Router = Backbone.Router.extend({
        currentPage:null,
        routes:
        {
            "" : "setRoot",
            "!" : "setRoot",
            "!articlelist": "setArticleList",
            "articlelist": "setArticleList",
            "!article_:id": "setArticle",
            "article_:id": "setArticle",
            "!t_:id": "setArticleByTitle",
            "t_:id": "setArticleByTitle",
            "!team" : "teamPage",
            "team" : "teamPage",
            "!learn" : "learnPage",
            "learn" : "learnPage",
            "!contact" : "contactPage",
            "contact" : "contactPage",
            "!policy" : "policyPage",
            "policy" : "policyPage",
            "!thankyou" : "thankyouPage",
            "thankyou" : "thankyouPage",
            "!getstarted" : "getStartedPage",
            "getstarted" : "getStartedPage",
            ":type/:id" : "itemDeepLink"
        },

        setRoot : function()
        {
            console.log("Setting front page");
            this.setActiveTab("blogTab");
            this.setPage(SR.Views.frontView);
        },

        setArticleList : function()
        {
            console.log("Setting articleList");
            this.setPage(SR.Views.articleList);
        },

        teamPage: function() {
            console.log("Setting team page");
            SR.Views.staticView.setTemplate("team");
            this.setActiveTab("teamTab");
            this.setPage(SR.Views.staticView);
        },

        policyPage: function() {
            console.log("Setting policy page");
            SR.Views.staticView.setTemplate("policy");
            $("ul.nav li a").removeClass("active");
            this.setPage(SR.Views.staticView);
        },

        thankyouPage: function() {
            console.log("Setting thankyou page");
            SR.Views.staticView.setTemplate("thankyou");
            $("ul.nav li a").removeClass("active");
            this.setPage(SR.Views.staticView);
        },

        contactPage: function() {
            console.log("Setting contact page");
            SR.Views.staticView.setTemplate("contact");
            this.setActiveTab("contactTab");
            this.setPage(SR.Views.staticView);
        },

        getStartedPage: function() {
            console.log("Setting contact page");
            SR.Views.staticView.setTemplate("getStarted");
            this.setActiveTab("getStartedTab");
            this.setPage(SR.Views.staticView);
        },

        learnPage: function() {
            console.log("Setting learn page");
            SR.Views.staticView.setTemplate("learn");
            this.setActiveTab("learnTab");
            this.setPage(SR.Views.staticView);
        },

        setArticle : function(id)
        {
            SR.Views.articleView.model = SR.Data.byNodeId[id];
            this.setActiveTab("blogTab");
            console.log("Setting article: "+id);
            this.setPage(SR.Views.articleView);
        },

        setArticleByTitle : function(id)
        {
            SR.Views.articleView.model = SR.Data.byTitle[id];
            this.setActiveTab("blogTab");
            console.log("Setting article: "+id);
            this.setPage(SR.Views.articleView);
        },

        setPage: function (page) {
            if (!page) {
                console.log("setPage(): No page passed in. Got: "+page);
                return;
            }

            if (this.currentPage) {
                this.currentPage.remove();
                this.currentPage.el = null;
            }

            var title = page.model != null ? page.model.get("title") : null;
            title = title != null ? title : "SaaSRadar | McKinsey & Company";

            $(document).attr('title', title);

            var content = $("#content");

            content.empty();
            page.el = content;
            page.render();
            this.currentPage = page;
        },

        refresh: function() {
            if (this.currentPage){
                this.currentPage.render();
            }
        },

        setActiveTab: function(tabid) {
            $("ul.nav li a").removeClass("active");
            $("ul.nav li a#"+tabid).addClass("active");
        }
    });

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function initViews() {

        Handlebars.registerHelper("debug", function(optionalValue) {
            console.log("Current Context");
            console.log("====================");
            console.log(this);

            if (optionalValue) {
                console.log("Value");
                console.log("====================");
                console.log(optionalValue);
            }
        });

        Handlebars.registerHelper('isTruthy', function (value, options) {
            if (value && value != "") {
                return options.fn(this);
            }
            return options.inverse(this);
        });

        Handlebars.registerPartial("article_block", Handlebars.getTemplate("article_block"));
        Handlebars.registerPartial("article_list", Handlebars.getTemplate("article_list"));

        if (SR.Data.articles.length > 0) {
            var firstArticle = SR.Data.articles.at(0);
            var lastArticle = firstArticle;

            SR.Data.byNodeId = {};
            SR.Data.byNodeId[firstArticle.get("node_id")] = firstArticle;
            SR.Data.byTitle = {};
            firstArticle.set("path", firstArticle.get("path").substr(1));
            SR.Data.byTitle[firstArticle.get("path")] = firstArticle;

            for (var i = 1; i < SR.Data.articles.length; i++) {
                var article = SR.Data.articles.at(i);
                var path = article.get("path").substr(1);
                lastArticle.nextArticle = path;
                SR.Data.byNodeId[article.get("node_id")] = article;
                article.set("path", path);
                SR.Data.byTitle[path] = article;
                lastArticle = article;
            }

            article.nextArticle = firstArticle.get("path");
        }

        SR.Views.frontView = new SR.View.FrontView({collection: SR.Data.articles});
        SR.Views.articleList = new SR.View.ArticleList({collection: SR.Data.articles});
        SR.Views.articleView = new SR.View.ArticleView();
        SR.Views.staticView = new SR.View.StaticView();

        SR.router = new SR.Router({pushState: true});

        var body = $("body");
        body.html(Handlebars.getTemplate("body")());
        body.addClass("main-body");

        var page = getParameterByName("page");

        if (page != null) {
            page = "#!"+page;
        } else {
            if (location.hash) {
                page = location.hash;
            } else {
                page = "";
            }
        }

        Backbone.history.start();

        if (page != "") {
            SR.router.navigate(page, {trigger: true});
        }
    }


    $(document).ready(function() {
        SR.Data.articles = new SR.Model.Articles();

        var shortUrl = "shortarticle-proxy.aspx";
        var fullUrl = "fullarticle-proxy.aspx";
        if (document.location.hostname == "localhost") {
            shortUrl = "http://www.saasradar-blog.com/short-articles.json";
            fullUrl = "http://www.saasradar-blog.com/full-articles.json";
        }
        SR.Data.articles.url = shortUrl;
        console.log("Loading short data: "+shortUrl);
        SR.Data.articles.fetch({
            contentType: "text/plain",
            success: function() {
                console.log("Got short articles!");
                SR.Data.firstLoad = true;
                initViews();
                var fullArticles = new SR.Model.Articles();
                fullArticles.url = fullUrl;
                fullArticles.fetch({
                    success: function() {
                        console.log("Got full articles!");
                        var lastArticle;
                        var firstArticlePath;

                        for (var i = 0; i < fullArticles.length; i++) {
                            var article = fullArticles.at(i);
                            var node_id = article.get("node_id");
                            var path = article.get("path").substr(1);
                            article.set("path", path);
                            var previous = SR.Data.articles.findWhere({node_id: node_id});

                            if (previous) {
                                previous.update(article);
                            } else {
                                SR.Data.articles.add(article);
                                SR.Data.byNodeId[node_id] = article;
                                SR.Data.byTitle[path] = article;
                            }

                            if (lastArticle) {
                                lastArticle.nextArticle = path;
                            }

                            lastArticle = article;

                            if (!firstArticlePath) {
                                firstArticlePath = path;
                            }

                            console.log("Path set to: "+path);

                            previous = null;
                        }

                        lastArticle.nextArticle = firstArticlePath;

                        fullArticles.reset();
                        fullArticles = null;

                        SR.Data.fullyLoaded = true;

                        SR.router.refresh();
                    }
                });

            }
        });
    });


})();
